<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">

  <!-- Category & Analysis specifications -->
  <Categories Default="Fluid Flow">
    <Cat>Fluid Flow</Cat>
  </Categories>

  <!-- Attribute definitions -->
  <Includes>
    <!-- Note: order is important -->
    <File>internal/templates/tabular-function.sbt</File>
    <File>internal/templates/analysis.sbt</File>
    <File>internal/templates/boundaryconditions.sbt</File>
    <File>internal/templates/material.sbt</File>
    <File>internal/templates/turbulence.sbt</File>
  </Includes>

  <!-- View specifications -->
  <Views>
    <View Type="Group" Title="SimBuilder" TopLevel="true">
      <Views>
        <View Title="Analysis"/>
        <View Title="Materials"/>
        <View Title="Boundary Conditions" />
        <View Title="Turbulence"/>
      </Views>
    </View>

    <View Type="Group" Title="Materials" Style="tiled">
      <Views>
        <View Title="Material" />
      </Views>
    </View>
    <View Type="Instanced" Title="Material">
      <InstancedAttributes>
        <Att Name="Material" Type="material" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="Analysis">
      <InstancedAttributes>
        <Att Name="Application" Type="application" />
        <Att Name="Time Control" Type="timeControl" />
        <Att Name="Data Writing" Type="dataWriting" />
        <Att Name="Other Control" Type="otherControl" />
        <Att Name="Time-Stepping Schemes" Type="ddtSchemes" />
        <Att Name="Gradient Schemes" Type="gradSchemes" />
        <Att Name="Pressure Solver" Type="psolver" />
        <Att Name="Velocity Solver" Type="usolver" />
        <Att Name="Initial Conditions" Type="initialCondition" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="Boundary Conditions">
      <AttributeTypes>
        <Att Type="boundaryCondition"/>
      </AttributeTypes>
    </View>

  </Views>

</SMTK_AttributeSystem>
