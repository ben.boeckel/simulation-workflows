<?xml version="1.0"?>
<SMTK_AttributeResource Version="3">
  <Definitions>
    <AttDef Type="ace3p-export" BaseType="operation" Label="Export to ACE3P" Version="1">
      <BriefDescription>
        Write ACE3P input file for selected program.
      </BriefDescription>
      <DetailedDescription>
      </DetailedDescription>
      <ItemDefinitions>
        <Resource Name="model" Label="Model" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" />
          </Accepts>
        </Resource>
        <Resource Name="attributes" Label="Attributes">
          <Accepts>
            <Resource Name="smtk::attribute::Resource"/>
          </Accepts>
        </Resource>
        <File Name="MeshFile" Label="Mesh File" ShouldExist="True"
          FileFilters="Exodus files (*.exo *.ex2 *.gen);;NetCDf files (*.ncdf);; All files (*.*)" Version="0"/>
        <String Name="Analysis" Label="ACE3P Analysis" Version="0">
          <ChildrenDefinitions>
            <Directory Name="OutputFolder" Label="Export Folder" Version="0">
              <BriefDescription>The folder to use on the local filesystem</BriefDescription>
            </Directory>
            <String Name="OutputFilePrefix" Label="Filename prefix" Version="0">
              <BriefDescription>The prefix to use for generated files</BriefDescription>
            </String>
            <String Name="AcdtoolTask" Label="Acdtool Task" Version="0">
              <ChildrenDefinitions>
                <String Name="AcdtoolMesh" Label="Mesh Task" Version="0">
                  <ChildrenDefinitions>
                    <String Name="OutputFilename" Label="Output filename" Version="0">
                      <BriefDescription>Filename for the generated .ncdf file</BriefDescription>
                      <DefaultValue>outputmesh.ncdf</DefaultValue>
                    </String>
                  </ChildrenDefinitions>
                  <DiscreteInfo>
                    <Value Enum="Stats">stats</Value>
                    <Value Enum="Check">check</Value>
                    <Structure>
                      <Value Enum="Fix">fix</Value>
                      <Items>
                        <Item>OutputFilename</Item>
                      </Items>
                    </Structure>
                  </DiscreteInfo>
                </String>
              </ChildrenDefinitions>
              <DiscreteInfo>
                <Value Enum="meshconvert">meshconvert</Value>
                <Structure>
                  <Value Enum="mesh">mesh</Value>
                  <Items>
                    <Item>AcdtoolMesh</Item>
                  </Items>
                </Structure>
                <!--
                <Value Enum="postprocess">postprocess</Value>
              -->
              </DiscreteInfo>
            </String>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Value Enum="">none</Value>
            <Structure>
              <Value Enum="Acdtool">acdtool</Value>
              <Items>
                <Item>AcdtoolTask</Item>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Omega3P">omega3p</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="S3P">s3p</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="T3P">t3p</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Track3P">track3p</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P Eigenmode">tem3p-eigen</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P Elastic">tem3p-structure</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P Harmonic Response">tem3p-harmonic</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P Thermal Linear">tem3p-thermal-linear</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P Thermal Nonlinear">tem3p-thermal-nonlinear</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="TEM3P ThermoElastic">tem3p-thermo-elastic</Value>
              <Items>
                <Item>OutputFolder</Item>
                <Item>OutputFilePrefix</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>

        <Group Name="NERSCSimulation" Label="Submit job to NERSC"
               Optional="true" IsEnabledByDefault="false"
               Version="1" NumberOfRequiredGroups="1">
          <ItemDefinitions>
            <String Name="JobName" Label="Job name" Version="0">
              <BriefDescription>Label you can use to track your job</BriefDescription>
              <DefaultValue>ACE3P</DefaultValue>
            </String>
            <String Name="JobNotes" Label="Notes" Version="0" MultipleLines="true">
              <BriefDescription>Optional notes you want to save with this job</BriefDescription>
              <DefaultValue> </DefaultValue>
            </String>
            <String Name="CumulusHost" Label="Cumulus host" Version="0">
              <DefaultValue>http://localhost:8080</DefaultValue>
            </String>
            <String Name="NERSCRepository" Label="Project repository" Version="0" />
            <String Name="NERSCAccountName" Label="NERSC account name" Version="0" />
            <String Name="NERSCAccountPassword" Label="NERSC account password"
                    Secure="true" Version="0" />
            <String Name="Machine" Label="NERSC Machine" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Cori (Cray XC40)">cori</Value>
                <Value Enum="Edison (Cray XC30)">edison</Value>
              </DiscreteInfo>
            </String>
            <Group Name="JobDirectory" Label="Job directory" Version="1" NumberOfRequiredGroups="1" Enabled="true">
              <BriefDescription>The working directory to use on the NERSC machine.</BriefDescription>
              <ItemDefinitions>
                <String Name="FileSystem" Label="File System" Version="0">
                  <ChildrenDefinitions>
                    <String Name="SubFolder" Label="Subfolder"  Version="0">
                      <BriefDescription>Relative path from user's $SCRATCH directory</BriefDescription>
                    </String>
                    <Void Name="AppendJobNameFolder" Label="Append Job Name Folder" Version="0"
                      Optional="true" IsEnabledByDefault="true">
                      <BriefDescription>Adds a folder with the job name to the specified path</BriefDescription>
                    </Void>
                    <String Name="FullPath" Label="Full Path"  Version="0">
                      <BriefDescription>Absolute path on NERSC machine</BriefDescription>
                    </String>
                  </ChildrenDefinitions>
                  <DiscreteInfo DefaultIndex = "0">
                    <Structure>
                      <Value Enum="$SCRATCH">scratch</Value>
                      <Items>
                        <Item>SubFolder</Item>
                        <Item>AppendJobNameFolder</Item>
                      </Items>
                    </Structure>
                    <Structure>
                      <Value Enum="Other">other</Value>
                      <Items>
                        <Item>FullPath</Item>
                      </Items>
                    </Structure>
                  </DiscreteInfo>
                </String>
              </ItemDefinitions>
            </Group>
            <String Name="Queue" Label="Queue" Version="0">
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="debug">debug</Value>
                <Value Enum="regular">normal</Value>
                <Value Enum="premium">premium</Value>
                <Value Enum="low priority">low</Value>
                <Value Enum="scavenger">scavenger</Value>
              </DiscreteInfo>
            </String>
            <Int Name="NumberOfNodes" Label="Number of nodes" Version="0">
              <DefaultValue>1</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <!--  Note that SLAC calls this number of "cores" -->
            <Int Name="NumberOfTasks" Label="Number of cores" Version="0">
              <BriefDescription>Number of processes per node</BriefDescription>
              <DefaultValue>1</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <Int Name="Timeout" Label="Time limit" Units="min" Version="0">
              <DefaultValue>5</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
<!--             <String Name="TailFile" Label="Tail Filename" Version="0" AdvanceLevel="1">
            </String>
 -->          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
     </AttDef>
   </Definitions>
  <Views>
    <View Type="Instanced" Title="Export Settings" TopLevel="true" FilterByCategory="false" FilterByAdvanceLevel="true">
      <InstancedAttributes>
        <Att Name="Options" Type="ExportSpec" />
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
