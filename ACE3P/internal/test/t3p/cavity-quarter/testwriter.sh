#!/usr/bin/env bash

# Hard-coded for jat desktop (turtleland2)

# Get directory, per http://stackoverflow.com/questions/59895
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  TARGET="$(readlink "$SOURCE")"
  if [[ $TARGET == /* ]]; then
    #echo "SOURCE '$SOURCE' is an absolute symlink to '$TARGET'"
    SOURCE="$TARGET"
  else
    DIR="$( dirname "$SOURCE" )"
    #echo "SOURCE '$SOURCE' is a relative symlink to '$TARGET' (relative to '$DIR')"
    SOURCE="$DIR/$TARGET" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
  fi
done
#echo "SOURCE is '$SOURCE'"
RDIR="$( dirname "$SOURCE" )"
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
# if [ "$DIR" != "$RDIR" ]; then
#   echo "DIR '$RDIR' resolves to '$DIR'"
# fi
echo "script DIR is '$DIR'"

test_num=2
if [ $# -gt 0 ] ; then
  test_num=$1
fi

PYTHON_EXE=${HOME}/projects/slac/build/cmb-superbuild/install/bin/pvpython

LD_LIBRARY_PATH=${HOME}/projects/slac/build/cmb-superbuild/install/lib \
PYTHONPATH=${HOME}/projects/slac/build/cmb-superbuild/superbuild/smtk/build \
${PYTHON_EXE} ${DIR}/testwriter.py \
  ${DIR}/t3p-test${test_num}.crf  \
  t3p  \
  ${DIR}/pillboxwg.smtk  \
  $*

#  ${DIR}/pillbox4-baseline${test_num}.o3p \
