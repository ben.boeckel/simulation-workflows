<?xml version="1.0"?>
<SMTK_AttributeSystem Version="2">
  <Categories>
    <Cat>T3P</Cat>
  </Categories>

  <Definitions>
    <AttDef Type="Monitor"
            Label="Monitor"
            BaseType="" Unique="true" Abstract="true"/>
    <AttDef Type="PointMonitor"
            Label="Point Monitor"
            BaseType="Monitor" Unique="true">
      <ItemDefinitions>
        <String Name="Name">
          <BriefDescription>The name of the file storing the fields as a function of time</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </String>
        <Double Name="Point" Label="Monitoring Point" NumberOfRequiredValues="3" Units="m">
          <BriefDescription>Location where the fields are monitored</BriefDescription>
          <ComponentLabels>
            <Label>X</Label>
            <Label>Y</Label>
            <Label>Z</Label>
          </ComponentLabels>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="VolumeMonitor"
            Label="Volume Monitor"
            BaseType="Monitor" Unique="true">
      <ItemDefinitions>
        <String Name="Name">
          <BriefDescription>The name of the files storing the snapshots of fields at a regular interval</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </String>
        <Double Name="TimeStart" Label="Start Time" Units="s">
          <BriefDescription>Time when recording fields starts</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
        <Double Name="TimeEnd" Label="End Time" Units="s">
          <BriefDescription>Time when recording fields ends</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
        <Double Name="TimeStep" Label="Time Interval" Units="s">
          <BriefDescription>Time interval for recornding fields</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="WakeFieldMonitor"
            Label="WakeField Monitor"
            BaseType="Monitor" Unique="true">
      <ItemDefinitions>
        <String Name="Name">
          <BriefDescription>The name of the file storing the wakefield as a function of s, which is
the distance from the front of the bunch and greater than 0</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </String>
        <Double Name="StartContour" Label="Start Contour" Units="m" Optional="true">
          <BriefDescription>The position where wakefield integration starts.  By default it is at beginning of the domain in z</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
        </Double>
        <Double Name="EndContour" Label="End Contour" Units="m" Optional="true">
          <BriefDescription>The position where wakefield integration stops.  By default it is at end of the domain in z</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
        </Double>
        <Double Name="Smax" Label="Max Distance" Units="m">
          <BriefDescription>(Smax) The maximum distance the wakefield is calculated</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="PowerMonitor"
            Label="Power Monitor"
            BaseType="Monitor" Unique="true">
      <ItemDefinitions>
        <String Name="Name">
          <BriefDescription>The name of the file storing the power as a function of time</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </String>
        <ModelEntity Name="MonitorBoundary" Label="Monitor Boundary" NumberOfRequiredValues="1">
          <MembershipMask>face</MembershipMask>
          <BriefDescription>The surface where the power is monitored</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </ModelEntity>
        <Double Name="TimeStart" Label="Start Time" Units="s">
          <BriefDescription>Time when recording power starts</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <DefaultValue>0.0</DefaultValue>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
        <Double Name="TimeEnd" Label="End Time" Units="s" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Time when recording power ends. If not enabled here, analysis end time is used.</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
        <Double Name="TimeStep" Label="Time Interval" Units="s" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>Time interval for recornding power. If not enabled here, analysis time interval is used.</BriefDescription>
          <Categories>
             <Cat>T3P</Cat>
          </Categories>
          <RangeInfo><Min Inclusive="true">0</Min></RangeInfo>
        </Double>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="ModeVoltageMonitor" Label="Mode Voltage Monitor" BaseType="Monitor" Unique="true">
      <ItemDefinitions>
        <String Name="Name">
          <BriefDescription>The name of the file storing the generalized voltage of a waveguide mode as a function of time</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </String>
        <ModelEntity Name="MonitorPort" Label="Monitor Port" NumberOfRequiredValues="1">
          <MembershipMask>face</MembershipMask>
          <BriefDescription>The port where the voltage is monitored</BriefDescription>
          <Categories>
            <Cat>T3P</Cat>
          </Categories>
        </ModelEntity>
        <Group Name="ESolver" Label="ESolver" Version="0">
          <ItemDefinitions>
            <String Name="Type" Label="Type" Version="0">
              <BriefDescription>Solver of the port mode</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DiscreteInfo DefaultIndex="0">
                <Value Enum="Numerical2D">Numerical2D</Value>
              </DiscreteInfo>
            </String>
            <Int Name="NumberOfModes" Label="Number Of Modes" Version="0">
              <BriefDescription>Number of waveguide modes to be solved</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
            <Double Name="Frequency" Label="Frequency" Version="0" Units="Hz">
              <BriefDescription>The frequency where the mode solutions are computer</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0e9</DefaultValue>
              <RangeInfo><Min Inclusive="true">0.0</Min></RangeInfo>
            </Double>
            <Double Name="Tolerance" Label="Tolerance" Version="0">
              <BriefDescription>The tolerance for solution convergence</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1.0e-15</DefaultValue>
              <RangeInfo><Min Inclusive="false">0.0</Min></RangeInfo>
            </Double>
            <Int Name="MaxIterations" Label="Max Iterations" Version="0">
              <BriefDescription>Maximum number of iterations allowed for solution convergence</BriefDescription>
              <Categories>
                <Cat>T3P</Cat>
              </Categories>
              <DefaultValue>1000</DefaultValue>
              <RangeInfo><Min Inclusive="true">1</Min></RangeInfo>
            </Int>
          </ItemDefinitions>
        </Group>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeSystem>
