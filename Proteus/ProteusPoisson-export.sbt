<SMTK_AttributeSystem Version="2">
  <Definitions>
    <AttDef Type="ExportSpec" BaseType="" Version="0" Unique="true">
      <ItemDefinitions>
        <File Name="NumericsFile" Label="Numerics output file (*_n.py)"
              NumberOfRequiredValues="1"
              FileFilters="Python files (*.py)">
          <DefaultValue>poisson_n.py</DefaultValue>
        </File>
        <File Name="PythonScript" Label="Python script" Version="0"
              NumberOfRequiredValues="1"
              AdvanceLevel="0" ShouldExist="true"
              FileFilters="Python files (*.py);;All files (*.*)">
          <DefaultValue>ProteusPoisson.py</DefaultValue>
        </File>
        <File Name="polyfile" Label="polyfile" Version="0"
              AdvanceLevel="1" NumberOfRequiredValues="1"
              Optional="true" IsEnabledByDefault="false"
              FileFilters="Piecewise Linear Complex files (*.poly);;All files (*.*)">
        </File>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
  <Attributes />

  <Views>
    <View Type="Instanced" Title="ExportSpec" TopLevel="true"
      FilterByAdvanceLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="Export Specification" Type="ExportSpec">Options</Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeSystem>
