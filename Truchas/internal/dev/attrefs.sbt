<SMTK_AttributeResource Version="3">
  <!--
  This example shows the syntax for <Component> definitions, which
  replace the <AttributeRef> defintions, which do not appear to
  display any more.
  -->

  <Definitions>
    <AttDef Type="material">
     </AttDef>

     <!-- AttributeRef items do NOT appear -->
    <AttDef Type="material-attref">
      <ItemDefinitions>
        <AttributeRef Name="material-ref" NumberOfRequiredValues="1">
          <AttDef>material</AttDef>
        </AttributeRef>
      </ItemDefinitions>
    </AttDef>

    <!-- Component items DO appear -->
    <AttDef Type="material-comp">
      <ItemDefinitions>
        <Component Name="att" NumberOfRequiredValues="1">
          <Accepts>
            <!-- Note that the predicate [type='...'] is REQUIRED -->
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material']" />
          </Accepts>
        </Component>
      </ItemDefinitions>
    </AttDef>

    <!-- Reference items do NOT appear -->
    <AttDef Type="material-ref">
      <ItemDefinitions>
        <Reference Name="att" NumberOfRequiredValues="1">
          <Accepts>
            <Resource Name="smtk::attribute::Resource" Filter="attribute[type='material']" />
          </Accepts>
        </Reference>
      </ItemDefinitions>
    </AttDef>

    <AttDef Type="data">
      <ItemDefinitions>
        <Double Name="Some Data">
          <DefaultValue>3.14159</DefaultValue>
        </Double>
      </ItemDefinitions>
    </AttDef>

  </Definitions>


  <Views>
    <View Type="Group" Title="AttRefs" TopLevel="true">
      <Views>
        <View Title="Material" />
        <View Title="InstanceAttRef" />
        <View Title="InstanceComp" />
        <View Title="InstanceRef" />
      </Views>
    </View>

    <View Type="Attribute" Title="Material">
      <AttributeTypes>
        <Att Type="material" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="InstanceAttRef">
      <InstancedAttributes>
        <Att Name="Ref" Type="material-attref" />
        <Att Name="Data" Type="data" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="InstanceComp">
      <InstancedAttributes>
        <Att Name="Comp" Type="material-comp" />
        <Att Name="Data" Type="data" />
      </InstancedAttributes>
    </View>

    <View Type="Instanced" Title="InstanceRef">
      <InstancedAttributes>
        <Att Name="Ref" Type="material-ref" />
        <Att Name="Data" Type="data" />
      </InstancedAttributes>
    </View>

  </Views>

</SMTK_AttributeResource>
