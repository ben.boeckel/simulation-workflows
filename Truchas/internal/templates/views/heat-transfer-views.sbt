<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeSystem Version="2">
  <Views>
    <View Type="Group" Title="Heat Transfer" TabPosition="North" TabIcons="false">
      <Views>
        <View Title="HT Surface Conditions" />
        <View Title="HT Solver" />
        <View Title="HT Sources" />
      </Views>
    </View>

    <View Type="Group" Title="HT Surface Conditions" Label="Boundary Conditions" TabPosition="North" TabIcon="false">
      <Views>
        <View Title="HT Boundary Conditions"/>
        <View Title="HT Interface Conditions"/>
      </Views>
    </View>

    <View Type="Attribute" Title="HT Boundary Conditions" Label="Boundary">
      <AttributeTypes>
        <Att Type="ht.boundary"/>
      </AttributeTypes>
    </View>

    <View Type="Attribute" Title="HT Interface Conditions" Label="Interface">
      <AttributeTypes>
        <Att Type="ht.interface"/>
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="HT Solver" Label="Solver">
      <InstancedAttributes>
        <Att Name="Solver" Type="ht.solver" />
      </InstancedAttributes>
    </View>

    <View Type="Attribute" Title="HT Sources" Label="Sources">
      <AttributeTypes>
        <Att Type="ht.source" />
      </AttributeTypes>
    </View>
  </Views>
</SMTK_AttributeSystem>
