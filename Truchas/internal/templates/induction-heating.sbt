<SMTK_AttributeResource Version="3">
  <Categories>
    <Cat>Induction Heating</Cat>
  </Categories>
  <Definitions>
    <AttDef Type="electromagnetics" Label="Electromagnetics" BaseType="" Version="0">
      <ItemDefinitions>
        <Component Name="model" Label="Geometry" LockType="DoNotLock">
          <Accepts>
            <Resource Name="smtk::model::Resource" Filter="model"/>
          </Accepts>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
        </Component>
        <Double Name="coordinate-scale-factor" Label="Coordinate Scale Factor" Version="0" Optional="true" IsEnabledByDefault="false">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>1.0</DefaultValue>
        </Double>
        <String Name="em-domain-type" Label="Domain Type" Version="0">
          <BriefDescription>A flag specifying the type of domain geometry that is discretizedby the computational mesh.</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DiscreteInfo>
            <Value Enum="Full Cylinder">full-cylinder</Value>
            <Value Enum="Half Cylinder">half-cylinder</Value>
            <Value Enum="Quarter Cylinder">quarter-cylinder</Value>
          </DiscreteInfo>
        </String>
        <String Name="symmetry-axis" Label="Symmetrix Axis" Verion="0">
          <BriefDescription>A flag that specifies which axis is to be used as the problemsymmetry axis for the Joule heat simulation.</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="2">
            <Value Enum="x">x</Value>
            <Value Enum="y">y</Value>
            <Value Enum="z">z</Value>
          </DiscreteInfo>
        </String>
        <Double Name="cg-stopping-tolerance" Label="CG Stopping Tolerance" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>1e-8</DefaultValue>
        </Double>
        <Int Name="maximum-cg-iterations" Label="Maximum CG Iterations" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>500</DefaultValue>
        </Int>
        <Double Name="num-etasq" Label="Num Etasq" AdvanceLevel="1" Version="0">
          <BriefDescription>Used for the displacement current coefficient in the lower-frequency,nondimensional scaling of Maxwell's equations, when its value exceeds the physical value.</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>1e-10</DefaultValue>
          <RangeInfo>
            <Min inclusive="true">0.0</Min>
          </RangeInfo>
        </Double>
        <Int Name="steps-per-cycle" Label="Steps Per Cycle" AdvanceLevel="1" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>20</DefaultValue>
        </Int>
        <Double Name="ss-stopping-tolerance" Label="SS Stopping Tolerance" AdvanceLevel="1" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>0.01</DefaultValue>
        </Double>
        <Int Name="maximum-source-cycles" Label="Maximum Source Cycles" AdvanceLevel="1" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DefaultValue>5</DefaultValue>
        </Int>
        <Int Name="output-level" Label="Output Level" AdvanceLevel="1" Version="0">
          <BriefDescription>Controls the verbostiy of the electtromagnetic solver</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <DiscreteInfo DefaultIndex="1">
            <Value>1</Value>
            <Value>2</Value>
            <Value>3</Value>
            <Value>4</Value>
          </DiscreteInfo>
        </Int>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="em-source" Label="EM Source" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="source-type" Label="Type" Version="0">
          <BriefDescription>Time-dependent source is not yet supported.</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Double Name="constant-source-frequency" Label="Frequency">
              <BriefDescription>Frequency (cycles per unit time) of the sinusoidally-varyingmagnetic source fields that drive the Joule heat calculation.</BriefDescription>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
            </Double>
            <Double Name="constant-uniform-source" Label="Uniform Source" AdvanceLevel="1" Version="0">
              <BriefDescription>Amplitude of a sinusiodal-varying, uniform magnetic source field thatdrives the Joule heat computation.</BriefDescription>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Constant">constant</Value>
              <Items>
                <Item>constant-source-frequency</Item>
                <Item>constant-uniform-source</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
      </ItemDefinitions>
    </AttDef>
    <AttDef Type="induction-coil" Label="Induction Coil" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="type" Label="Type" Version="0">
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
          <ChildrenDefinitions>
            <Int Name="nturns" Label="NTurns" Version="0">
              <BriefDescription>Number of turns of the coil</BriefDescription>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="true">2</Min>
              </RangeInfo>
            </Int>
            <Double Name="radius" Label="Radius" Version="0">
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="length" Label="Length" Version="0">
              <BriefDescription>Length of the coil</BriefDescription>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <RangeInfo>
                <Min Inclusive="false">0.0</Min>
              </RangeInfo>
            </Double>
            <Double Name="center" Label="Center" NumberOfRequiredValues="3" Version="0">
              <BriefDescription>The position of the center of the coil</BriefDescription>
              <Categories>
                <Cat>Induction Heating</Cat>
              </Categories>
              <DefaultValue>0.0</DefaultValue>
            </Double>
          </ChildrenDefinitions>
          <DiscreteInfo DefaultIndex="0">
            <Structure>
              <Value Enum="Multiple Turn">multiple</Value>
              <Items>
                <Item>nturns</Item>
                <Item>center</Item>
                <Item>radius</Item>
                <Item>length</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Single Turn">single</Value>
              <Items>
                <Item>center</Item>
                <Item>radius</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>
        <Double Name="current" Label="Current" NumberOfRequiredValues="1" Version="0">
          <BriefDescription>Amplitude of the sinusoidally-varying current in the coil</BriefDescription>
          <Categories>
            <Cat>Induction Heating</Cat>
          </Categories>
        </Double>
      </ItemDefinitions>
    </AttDef>
  </Definitions>
</SMTK_AttributeResource>
