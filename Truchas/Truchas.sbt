<?xml version="1.0" encoding="utf-8" ?>
<SMTK_AttributeResource Version="3">

  <!-- Category & Analysis specifications -->
  <Categories>
    <Cat>Enclosure Radiation</Cat>
    <Cat>Fluid Flow</Cat>
    <Cat>Heat Transfer</Cat>
    <Cat>Induction Heating</Cat>
    <Cat>Solid Mechanics</Cat>
  </Categories>

  <Analyses>
    <Analysis Type="Heat Transfer">
      <Cat>Heat Transfer</Cat>
    </Analysis>
    <Analysis Type="Enclosure Radiation" BaseType="Heat Transfer">
      <Cat>Enclosure Radiation</Cat>
    </Analysis>
    <Analysis Type="Induction Heating" BaseType="Heat Transfer">
      <Cat>Induction Heating</Cat>
    </Analysis>
    <Analysis Type="Fluid Flow">
      <Cat>Fluid Flow</Cat>
    </Analysis>
    <Analysis Type="Solid Mechanics">
      <Cat>Solid Mechanics</Cat>
    </Analysis>
  </Analyses>

  <!-- Attribute definitions -->
  <Includes>
    <!-- Note: order is important, e.g., put expressions first -->
    <File>internal/templates/functions.sbt</File>
    <File>internal/templates/tabular-function.sbt</File>
    <File>internal/templates/material.sbt</File>
    <File>internal/templates/thermal-surface-condition.sbt</File>
    <File>internal/templates/boundary-condition.sbt</File>
    <File>internal/templates/fluid-numerics.sbt</File>
    <File>internal/templates/numerics-solver.sbt</File>
    <File>internal/templates/body-source-probe.sbt</File>
    <File>internal/templates/enclosure.sbt</File>
    <File>internal/templates/other.sbt</File>
    <File>internal/templates/initial-conditions.sbt</File>
    <File>internal/templates/induction-heating.sbt</File>

    <File>internal/templates/views/fluid-flow-views.sbt</File>
    <File>internal/templates/views/heat-transfer-views.sbt</File>
  </Includes>

  <Definitions>
    <AttDef Type="description" Label="Description" BaseType="" Version="0">
      <ItemDefinitions>
        <String Name="description" Label="text:" MultipleLines="true">
          <BriefDescription>Text added to top of input file</BriefDescription>
          <Categories>
            <Cat>Enclosure Radiation</Cat>
            <Cat>Fluid Flow</Cat>
            <Cat>Heat Transfer</Cat>
            <Cat>Induction Heating</Cat>
            <Cat>Solid Mechanics</Cat>
          </Categories>
          <DefaultValue># Truchas simulation</DefaultValue>
        </String>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <!-- Attribute exclusion specifications -->
  <Exclusions>
    <!-- HT BCs are either exterior (boundary) or interface -->
    <Rule>
      <Def>ht.boundary</Def>
      <Def>ht.interface</Def>
    </Rule>

    <!-- HT Dirichlet BCs are exclusive to other exterior BCs -->
    <!-- HT Flux BCs are exclusinve to HTC and Radiation BCs-->
    <Rule>
      <Def>ht.boundary.dirichlet</Def>
      <Def>ht.boundary.flux</Def>
      <Def>ht.boundary.HTC</Def>
    </Rule>
    <Rule>
      <Def>ht.boundary.dirichlet</Def>
      <Def>ht.boundary.flux</Def>
      <Def>ht.boundary.radiation</Def>
    </Rule>
  </Exclusions>

  <!-- View specifications -->
  <Views>
    <View Type="Group" Title="TopLevel" TopLevel="true" TabPosition="North"
      FilterByAdvanceLevel="true" FilterByCategory="false">
      <Views>
        <View Title="Analysis" />
        <View Title="Materials" />
        <View Title="Modules" />
        <View Title="Body" />
        <View Title="Globals" />
        <View Title="Other" />
      </Views>
    </View>

    <View Type="Instanced" Name="Description">
      <InstancedAttributes>
        <Att Name="description" Type="description" />
      </InstancedAttributes>
    </View>


    <View Type="Group" Title="Analysis" Style="Tiled">
      <Views>
        <View Title="SelectModules" />
        <View Title="Description" />
        <View Title="General" />
      </Views>
    </View>

    <View Type="Analysis" Name="SelectModules" Label="Select Modules"
      AnalysisAttributeName="analysis" AnalysisAttributeType="analysis">
    </View>

    <View Type="Group" Name="Modules" Label="Modules" TabPosition="North">
      <Views>
        <View Title="Enclosure Radiation" />
        <View Title="Fluid Flow" />         <!-- In views/fluid-flow-views.sbt -->
        <View Title="Heat Transfer" />      <!-- In views/heat-transfer-views.sbt -->
        <View Title="Induction Heating" />
        <View Title="Solid Mechanics" />
      </Views>
    </View>

    <View Type="Instanced" Name="Enclosure Radiation">
      <InstancedAttributes>
        <Att Name="enclosure-radiation" Type="enclosure-radiation" />
      </InstancedAttributes>
    </View>

    <View Type="Group" Name="Induction Heating" TabPosition="North">
      <Views>
        <View Title="Electromagnetics" />
        <View Title="EM Sources" />
      </Views>
    </View>
    <View Type="Instanced" Title="Electromagnetics">
      <InstancedAttributes>
        <Att Name="electromagnetics" Type="electromagnetics" />
      </InstancedAttributes>
    </View>
    <View Type="Group" Name="EM Sources" Style="Tiled">
      <Views>
        <View Title="Source" />
        <View Title="Induction Coils" />
      </Views>
    </View>
    <View Type="Instanced" Title="Source">
      <InstancedAttributes>
        <Att Name="em-source" Type="em-source" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Induction Coils">
      <AttributeTypes>
        <Att Type="induction-coil" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Name="Solid Mechanics">
      <InstancedAttributes>
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Materials" TabPosition="North">
      <Views>
        <View Title="Phases" />
        <View Title="Phase Transitions" />
        <View Title="Material Assignment Group" />
        <View Title="Material Functions" />
      </Views>
    </View>
    <View Type="Attribute" Title="Phases" HideAssociations="true">
      <AttributeTypes>
        <Att Type="phase.material"/>
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Phase Transitions" HideAssociations="true">
      <AttributeTypes>
        <Att Type="phase-transition"/>
      </AttributeTypes>
    </View>
    <View Type="Group" Name="Material Assignment Group" Label="Assignment" Style="Tiled">
      <Views>
        <View Title="Background Material" />
        <View Title="Void Material" />
        <View Title="Material Assignment" />
      </Views>
    </View>
    <View Type="Instanced" Title="Background Material" Label="Default Material">
      <InstancedAttributes>
        <Att Name="Background Material" Type="background-material" />
      </InstancedAttributes>
    </View>
    <View Type="Instanced" Title="Void Material" Label="Void Material">
      <InstancedAttributes>
        <Att Name="Void Material" Type="phase.void" />
      </InstancedAttributes>
    </View>
    <View Type="Associations" Title="Material Assignment" Label="Element Block Assignment">
      <AttributeTypes>
        <Att Type="phase" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Material Functions" Label="Functions">
      <AttributeTypes>
        <Att Type="fn.material" />
      </AttributeTypes>
    </View>

    <View Type="Group" Title="Body" Label="Initial Conditions" TabPosition="North">
      <Views>
        <View Title="Temperature IC" />
        <View Title="Velocity IC" />
        <View Title="Initial Condition Functions" />
      </Views>
    </View>
    <View Type="Attribute" Name="Temperature IC" Label="Temperature">
      <AttributeTypes>
        <Att Type="TemperatureInitialCondition" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Name="Velocity IC" Label="Velocity">
      <AttributeTypes>
        <Att Type="VelocityInitialCondition" />
      </AttributeTypes>
    </View>
    <View Type="Attribute" Title="Initial Condition Functions" Label="Functions">
      <AttributeTypes>
        <Att Type="fn.initial-condition" />
      </AttributeTypes>
    </View>

     <View Type="Group" Title="Other" Style="tiled">
      <Views>
        <View Title="Other Instanced" />
        <View Title="Probes" />
      </Views>
    </View>
    <View Type="Instanced" Title="Other Instanced" Label="Mesh">
      <InstancedAttributes>
        <Att Name="Mesh" Type="mesh" />
      </InstancedAttributes>
    </View>
    <View Type="Attribute" Title="Probes">
      <AttributeTypes>
        <Att Type="probe" />
      </AttributeTypes>
    </View>

    <View Type="Instanced" Title="General">
      <InstancedAttributes>
        <Att Name="numerics-att" Type="numerics" />
        <Att Name="outputs-att" Type="outputs" />
        <Att Name="simulation-control-att" Type="simulation-control" />
<!--         <Att Name="Mesh" Type="mesh" /> -->
      </InstancedAttributes>
    </View>

    <View Type="Group" Title="Globals" Style="Tiled">
      <Views>
        <View Title="Global Constants" />
      </Views>
    </View>

    <View Type="Instanced" Title="Global Constants">
      <InstancedAttributes>
        <Att Name="physics" Type="physics" />
        <Att Name="physical-constants" Type="physical-constants" />
      </InstancedAttributes>
    </View>

  </Views>

</SMTK_AttributeResource>
